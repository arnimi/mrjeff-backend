# MrJeff - Backend developer

#### Create docker image
mvn clean package <br />
docker build -t coupon . <br />
docker run -p 5000:8080 coupon <br />

### MicroService will be available at:
http://localhost:5000