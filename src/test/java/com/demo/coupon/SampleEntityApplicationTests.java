package com.demo.coupon;

import com.demo.coupon.entity.SampleEntity;
import com.demo.coupon.repository.SampleEntityRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class SampleEntityApplicationTests {

	@Autowired
	private SampleEntityRepository sampleEntityRepository;

	@Before
	public void setUp() {
		sampleEntityRepository.deleteAll();
	}

	@Test
	public void contextLoads() {
		Stream.of(
				new SampleEntity("TEST1", "TYPE1", "CHANNEL1" ),
				new SampleEntity( "TEST2", "TYPE2", "CHANNEL2"))
				.forEach(sampleEntityRepository::save);


		assertThat(sampleEntityRepository.count()).isEqualTo(2);
	}

}

