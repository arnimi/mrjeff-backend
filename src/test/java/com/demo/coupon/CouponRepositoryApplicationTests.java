package com.demo.coupon;

import com.demo.coupon.entity.CouponEntity;
import com.demo.coupon.entity.EnumChannelType;
import com.demo.coupon.entity.EnumCouponType;
import com.demo.coupon.repository.CouponEntityRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class CouponRepositoryApplicationTests {

	@Autowired
	private CouponEntityRepository couponEntityRepository;

	@Before
	public void setUp() {
		couponEntityRepository.deleteAll();

	}

	@Test
	public void contextLoads() {
		Stream.of(
				new CouponEntity("11111", 10, EnumCouponType.FIJO, EnumChannelType.LOCAL, 2))
				.forEach(couponEntityRepository::save);


		assertThat(couponEntityRepository.count()).isEqualTo(1);
	}

	@Test
	public void find() {
		couponEntityRepository.deleteAll();
		Stream.of(
				new CouponEntity("11111", 10, EnumCouponType.FIJO, EnumChannelType.LOCAL, 2))
				.forEach(couponEntityRepository::save);


		assertThat(couponEntityRepository.findByCode("11111").getDiscount()).isEqualTo(10);
	}

}

