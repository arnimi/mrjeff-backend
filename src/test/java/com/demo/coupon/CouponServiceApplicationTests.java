package com.demo.coupon;

import com.demo.coupon.entity.CouponEntity;
import com.demo.coupon.entity.EnumChannelType;
import com.demo.coupon.entity.EnumCouponType;
import com.demo.coupon.entity.ProductEntity;
import com.demo.coupon.repository.CouponEntityRepository;
import com.demo.coupon.response.TotalResponse;
import com.demo.coupon.service.CouponServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class CouponServiceApplicationTests {

	@Autowired
	private CouponEntityRepository couponEntityRepository;
	@Autowired
	private CouponServiceImpl couponServiceImpl;

	@Before
	public void setUp() {
		couponEntityRepository.deleteAll();
	}

	@Test
	public void calculateTotal() {

		Stream.of(
				new CouponEntity("11111", 10, EnumCouponType.FIJO, EnumChannelType.LOCAL, 2))
				.forEach(couponEntityRepository::save);

		List<ProductEntity> productEntityList = new ArrayList<>();
		productEntityList.add(new ProductEntity("aaaaa", 20));
		productEntityList.add(new ProductEntity("bbbbb", 30));
		productEntityList.add(new ProductEntity("ccccc", 40));

		TotalResponse totalResponse = couponServiceImpl.total(productEntityList, "11111");
		assertThat(totalResponse.getPrice()).isEqualTo(90);
		assertThat(totalResponse.getDiscount()).isEqualTo(10);
		assertThat(totalResponse.getTotal()).isEqualTo(80);
	}

	@Test
	public void coupons() {
		couponEntityRepository.deleteAll();
		Stream.of(
				new CouponEntity("11111", 10, EnumCouponType.FIJO, EnumChannelType.LOCAL, 2),
				new CouponEntity("22222", 20, EnumCouponType.PORCENTAJE, EnumChannelType.LOCAL, 4),
				new CouponEntity("33333", 30, EnumCouponType.PORCENTAJE, EnumChannelType.DELIVERY, 6))
				.forEach(couponEntityRepository::save);

		assertThat(couponServiceImpl.coupons().size()).isEqualTo(1);
	}

}

