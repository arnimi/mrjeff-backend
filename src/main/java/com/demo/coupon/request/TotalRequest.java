package com.demo.coupon.request;

import com.demo.coupon.entity.ProductEntity;

import javax.validation.constraints.NotNull;
import java.util.List;

public class TotalRequest {

    @NotNull private List<ProductEntity> products;
    private String couponCode;

    public TotalRequest(
            List<ProductEntity> products,
            String couponCode) {

        this.products = products;
        this.couponCode = couponCode;
    }

    public List<ProductEntity> getProducts() {
        return products;
    }

    public String getCouponCode() {
        return couponCode;
    }

    @Override
    public String toString() {
        return "TotalRequest{" +
                "products=" + products +
                ", coupon='" + couponCode + '\'' +
                '}';
    }
}