package com.demo.coupon.response;

public class TotalResponse {

    private Integer price;
    private Integer discount;
    private Integer total;

    public TotalResponse(
            Integer price,
            Integer discount,
            Integer total) {

        this.price = price;
        this.discount = discount;
        this.total = total;
    }

    public Integer getPrice() {
        return price;
    }

    public Integer getDiscount() {
        return discount;
    }

    public Integer getTotal() {
        return total;
    }
}