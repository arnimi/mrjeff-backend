package com.demo.coupon.controller;

import com.demo.coupon.request.TotalRequest;
import com.demo.coupon.service.CouponService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * REST controller.
 */
@RestController
@RequestMapping("/api")
public class CouponController {

    private final CouponService couponService;

    public CouponController(CouponService couponService) {
        this.couponService = couponService;
    }

    /**
     * POST  /total : get total of products.
     *
     * @param totalRequest request with products and discounts
     */
    @PostMapping("/total")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity total(@RequestBody TotalRequest totalRequest) {
        return ResponseEntity.ok(couponService.total(totalRequest.getProducts(), totalRequest.getCouponCode()));
    }

    /**
     * Get  /discounts : get all coupons.
     */
    @GetMapping("/coupons")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity coupons() {
        return ResponseEntity.ok(couponService.coupons());
    }
}
