package com.demo.coupon;

import com.demo.coupon.entity.SampleEntity;
import com.demo.coupon.repository.SampleEntityRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.stream.Stream;

@Component
@Profile("default")
class SampleEntityInitializator implements CommandLineRunner {

    private final SampleEntityRepository repository;

    public SampleEntityInitializator(SampleEntityRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(String... strings) throws Exception {
        Stream.of(
                new SampleEntity("TEST1", "TYPE1", "CHANNEL1"),
                new SampleEntity("TEST2", "TYPE2", "CHANNEL2"))
                .forEach(repository::save);

        repository.findAll().forEach(System.out::println);
    }
}