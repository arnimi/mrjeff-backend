package com.demo.coupon.service;

import com.demo.coupon.entity.CouponEntity;
import com.demo.coupon.entity.EnumChannelType;
import com.demo.coupon.entity.EnumCouponType;
import com.demo.coupon.entity.ProductEntity;
import com.demo.coupon.repository.CouponEntityRepository;
import com.demo.coupon.response.TotalResponse;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CouponServiceImpl implements CouponService {

    private final CouponEntityRepository couponEntityRepository;

    public CouponServiceImpl(CouponEntityRepository couponEntityRepository) {
        this.couponEntityRepository = couponEntityRepository;
    }

    @Override
    public TotalResponse total(List<ProductEntity> products, String coupon) {
        Integer price = products.stream().mapToInt(product -> product.getPrice()).sum();
        Integer total = price;
        Integer discount = 0;
        if(coupon != null) {
            CouponEntity couponEntity = couponEntityRepository.findByCode(coupon);
            if (price >= couponEntity.getPedidoMinimo()) {
                discount = couponEntity.getType().equals(EnumCouponType.PORCENTAJE.getCouponType()) ?
                        price * couponEntity.getDiscount() / 100 : couponEntity.getDiscount();
                total = price - discount;
            }
        }
        return new TotalResponse(price, discount, total);
    }

    @Override
    public List<CouponEntity> coupons() {
        return StreamSupport.stream(couponEntityRepository.findAll().spliterator(), false)
                .filter(coupon -> coupon.getType().equals(EnumCouponType.FIJO.getCouponType()) && coupon.getChannel().equals(EnumChannelType.LOCAL.getChannelType()))
                .collect(Collectors.toList());
    }

}
