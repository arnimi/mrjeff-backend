package com.demo.coupon.service;

import com.demo.coupon.entity.CouponEntity;
import com.demo.coupon.entity.ProductEntity;
import com.demo.coupon.response.TotalResponse;

import java.util.List;

public interface CouponService {

    /**
     * Total price of products.
     *
     * @param products given from request {@link List< ProductEntity >}
     * @param couponCode given from request
     *
     * @return total price of products
     */
    TotalResponse total(List<ProductEntity> products, String couponCode);

    /**
     * Coupons fijos y channel local.
     *
     * @return coupons
     */
    List<CouponEntity> coupons();
}
