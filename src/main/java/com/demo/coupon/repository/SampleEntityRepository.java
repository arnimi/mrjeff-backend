package com.demo.coupon.repository;

import com.demo.coupon.entity.SampleEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface SampleEntityRepository extends PagingAndSortingRepository<SampleEntity, Long> {
}
