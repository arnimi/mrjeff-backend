package com.demo.coupon.repository;

import com.demo.coupon.entity.CouponEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface CouponEntityRepository extends PagingAndSortingRepository<CouponEntity, Long> {

    CouponEntity findByCode(@Param("code") String code);
}
