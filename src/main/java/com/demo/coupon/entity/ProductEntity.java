package com.demo.coupon.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ProductEntity {

    @Id
    @GeneratedValue
    private Long id;
    private String code;
    private Integer price;

    public ProductEntity() {
    }

    public ProductEntity(String code, Integer price) {
        this.code = code;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public Integer getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "ProductEntity{" +
                "id=" + id + '\'' +
                "code=" + code + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}