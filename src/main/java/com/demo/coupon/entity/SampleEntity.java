package com.demo.coupon.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class SampleEntity {
    @Id
    @GeneratedValue
    private Long id;
    private String code;
    private String type;
    private String channel;

    public SampleEntity() {
    }

    public SampleEntity(String code, String type, String channel) {
        this.code = code;
        this.type = type;
        this.channel = channel;
    }

    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public String getChannel() {
        return channel;
    }

    @Override
    public String toString() {
        return "SampleEntity{" +
                "id=" + id + '\'' +
                ", code='" + code + '\'' +
                ", type='" + type + '\'' +
                ", channel='" + channel + '\'' +
                '}';
    }
}