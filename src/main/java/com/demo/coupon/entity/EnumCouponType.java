package com.demo.coupon.entity;

public enum EnumCouponType {
    FIJO("FIJO"),
    PORCENTAJE("PORCENTAJE");

    private final String couponType;

    /**
     * @param couponType
     */
    EnumCouponType(final String couponType) {
        this.couponType = couponType;
    }

    public String getCouponType() {
        return couponType;
    }
}
