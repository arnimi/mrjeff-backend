package com.demo.coupon.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class CouponEntity {

    @Id
    @GeneratedValue
    private Long id;
    private String code;
    private Integer discount;
    private EnumCouponType type;
    private EnumChannelType channel;
    private Integer pedidoMinimo;


    public CouponEntity() {
    }

    public CouponEntity(String code, Integer discount, EnumCouponType type, EnumChannelType channel, Integer pedidoMinimo) {
        this.code = code;
        this.discount = discount;
        this.type = type;
        this.channel = channel;
        this.pedidoMinimo = pedidoMinimo;
    }

    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public Integer getDiscount() {
        return discount;
    }

    public String getType() {
        return type.getCouponType();
    }

    public String getChannel() {
        return channel.getChannelType();
    }

    public Integer getPedidoMinimo() { return pedidoMinimo != null ? pedidoMinimo : 0; }

    @Override
    public String toString() {
        return "CouponEntity{" +
                "id=" + id + '\'' +
                ",code=" + code + '\'' +
                ",discount=" + discount + '\'' +
                ",type=" + type + '\'' +
                ",channel='" + channel + '\'' +
                ",pedido_minimo='" + pedidoMinimo + '\'' +
                '}';
    }
}