package com.demo.coupon.entity;

public enum EnumChannelType {
    LOCAL("LOCAL"),
    DELIVERY("DEVLIVERY");

    private final String channelType;

    /**
     * @param channelType
     */
    EnumChannelType(final String channelType) {
        this.channelType = channelType;
    }

    public String getChannelType() {
        return channelType;
    }
}
